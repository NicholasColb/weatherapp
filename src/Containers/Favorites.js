import Favorites from '../Components/Favorites'
import { connect } from 'react-redux'
import * as actions from '../Actions'

export function mapStateToProps({ favorites, loading }) {
  return {
    favorites: favorites,
    loading: loading
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    updateFavorites: (favorites) => dispatch(actions.updateFavorites(favorites)),
    deleteFavorite: (name) => dispatch(actions.deleteFavorite(name))
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
