import SearchAddress from '../Components/SearchAddress'
import { connect } from 'react-redux'
import * as actions from '../Actions'

export function mapDispatchToProps(dispatch) {
  return {
    onLocationChange: (lat, lon) => dispatch(actions.fetchWeather(lat, lon))
  }
}

export default connect(null, mapDispatchToProps)(SearchAddress);
