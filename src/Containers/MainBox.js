import MainBox from '../Components/MainBox'
import { connect } from 'react-redux'
import * as actions from '../Actions'

export function mapStateToProps({ currentWeather }) {
  return {
    currentWeather: currentWeather
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    onFavorite: () => dispatch(actions.addFavorite())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainBox);
