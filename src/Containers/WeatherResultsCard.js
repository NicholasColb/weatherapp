import WeatherResultsCard from '../Components/WeatherResultsCard'
import { connect } from 'react-redux'

export function mapStateToProps({ currentWeather }) {
  return {
    currentWeather: currentWeather
  }
}

export default connect(mapStateToProps)(WeatherResultsCard)
