import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import App from './Components/App'
import weatherReducer from './Reducers'
import './index.css'

import * as serviceWorker from './serviceWorker'

const persistConfig = {
  key: 'root',
  storage,
  blacklist: [ 'loading', 'currentWeather', 'error']
}

const persistedReducer = persistReducer(persistConfig, weatherReducer)

const store = createStore(
  persistedReducer,
  {
    favorites: [
    ],
    loading: false,
    currentWeather: null,
    error: null
  },
  applyMiddleware(thunk)
)

const persistor = persistStore(store);

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router>
        <Switch>
          <Route path="/" component={App} />
        </Switch>
      </Router>
    </PersistGate>
  </Provider>


  , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
