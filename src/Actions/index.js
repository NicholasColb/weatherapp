import {
  FETCH_WEATHER_BEGIN,
  FETCH_WEATHER_SUCCESS,
  FETCH_WEATHER_FAILURE,
  ADD_FAVORITE,
  DELETE_FAVORITE,
  UPDATE_FAVORITES_BEGIN,
  UPDATE_FAVORITES_SUCCESS,
  UPDATE_FAVORITES_FAILURE
} from '../Constants';

export const addFavorite = () => ({
  type: ADD_FAVORITE,
})

export const deleteFavorite = (name) => ({
  type: DELETE_FAVORITE,
  payload: {
    name
  }
})


export const fetchWeather = (lat, lon) => {
  return dispatch => {
    dispatch(fetchWeatherStarted())

    var encodedURL = (
      'http://api.openweathermap.org/data/2.5/weather?'
       + 'lat=' + lat + '&lon=' + lon + '&APPID=09ba4171b4bcb48548a7c13b088a909b'
    )

    fetch(encodedURL, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    .then(res => {
      res.json().then(res => {
        console.log(res)
        dispatch(fetchWeatherSuccess(res))
      })
    })
    .catch(err => dispatch(fetchWeatherFailure(err.message)))
  }
}

const fetchWeatherStarted = () =>({
  type: FETCH_WEATHER_BEGIN
})

const fetchWeatherFailure = error => ({
  type: FETCH_WEATHER_FAILURE,
  payload: {
    error
  }
})

const fetchWeatherSuccess = weather => ({
  type: FETCH_WEATHER_SUCCESS,
  payload: {
    ...weather
  }
});



export const updateFavorites = (favorites) => {
  return dispatch => {
    dispatch(updateFavoritesBegin())

    let updatedFavorites = []

    const update = i => {
      if(favorites[i]) {
        const fav = favorites[i]
        const encodedURL = (
          'http://api.openweathermap.org/data/2.5/weather?'
           + 'lat=' + fav.location.lat + '&lon=' + fav.location.lon + '&APPID=09ba4171b4bcb48548a7c13b088a909b'
        )
        fetch(encodedURL, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then(res => {
          res.json().then(res => {
            const updatedFavorite = {
              name: res.name,
              location: res.coord,
              iconId: res.weather[0].icon,
              temp: Math.round(res.main.temp - 273.15),
              type: res.weather[0].main
            }
            updatedFavorites.push(updatedFavorite)

            update(i + 1)
          })
        })
        .catch(err => dispatch(updateFavoritesFailure(err.message)))
      } else {
        //the favorites array has been looped over successfully
        dispatch(updateFavoritesSuccess(updatedFavorites))
      }
    }
    update(0)
  }
}

const updateFavoritesBegin = () => ({
  type: UPDATE_FAVORITES_BEGIN
})

const updateFavoritesFailure = error => ({
  type: UPDATE_FAVORITES_FAILURE,
  payload: {
    error
  }
})

const updateFavoritesSuccess = (favorites) => ({
  type: UPDATE_FAVORITES_SUCCESS,
  payload: {
    favorites: [...favorites ]
  }
})
