import React from 'react'
import { shallow } from 'enzyme'
import Favorites from '../Components/Favorites'
import WeatherResultsCard from '../Components/WeatherResultsCard'

describe('Favorites', () => {

  const onUpdateMock = jest.fn(),
        onDeleteMock = jest.fn(),
        weather1 = {
          name: "Vietnam",
          location: {lon: 25.56, lat: 53.33},
          iconId: "10d",
          temp: 34,
          type: "Clear"
        },
        weather2 = {
          name: "Somapah Serangoon",
          location: {lon: 103.87, lat: 1.36},
          temp: 27,
          iconId: "04n",
          type: "Clouds"
        };

  const wrapper = shallow(
    <Favorites
      favorites={[weather1, weather2]}
      loading={false}
      updateFavorites={onUpdateMock}
      deleteFavorite={onDeleteMock}
    />
  );



  it('should render the correct amount of favorites', () => {
        expect(wrapper.find(WeatherResultsCard)).toHaveLength(2);
  });

  it('should have called function to update the weathers of favorites', () => {
      expect(onUpdateMock).toHaveBeenCalled()
  })

  it('should trigger the onDelete function on click', () => {
    wrapper.find('button').first().simulate('click')
    expect(onDeleteMock).toHaveBeenCalled()
  })

  it('should render the correct data about the favorites', () => {
    const prop = wrapper.find('WeatherResultsCard').first().props().currentWeather
    expect(prop.name).toEqual("Vietnam")
    expect(prop.location).toEqual(weather1.location)
    expect(prop.iconId).toEqual(weather1.iconId)
    expect(prop.temp).toEqual(weather1.temp)
    expect(prop.type).toEqual(weather1.type)
  })

});
