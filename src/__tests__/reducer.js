import weatherReducer from '../Reducers'
import * as constants from '../Constants'

describe('Reducer functionalities', () => {


  const weather1 = {
    name: "Vietnam",
    location: {lon: 25.56, lat: 53.33},
    iconId: "10d",
    temp: 34,
    type: "Clear"
  },
  weather2 = {
    name: "Somapah Serangoon",
    location: {lon: 103.87, lat: 1.36},
    temp: 27,
    iconId: "04n",
    type: "Clouds"
  },
  mockRequestData2 = {
      name: "Somapah Serangoon",
      coord: {lon: 103.87, lat: 1.36},
      weather: [{icon: "04n", main: "Clouds"}],
      main: {temp: 300.15},
  }


  const initialState = {
    currentWeather: weather1,
    favorites: [
      weather2
    ],
    loading: false,
    error: null,
  }

  const endStateEmpty = {
    ...initialState,
    favorites: []
  }

  const endStateFull = {
    ...initialState,
    favorites: [
      weather2,
      weather1
    ]
  }
  const endStateUpdated = {
    ...initialState,
    currentWeather: weather2,

  }
  const reducerAction = (action) => {
    return weatherReducer(initialState, action)
  }


  it('should have the correct number of favorites after deletion', () => {
    expect(reducerAction({})).toEqual(initialState)
    expect(reducerAction({
      type: constants.DELETE_FAVORITE,
      payload: {name: 'Somapah Serangoon'}
    })).toEqual(endStateEmpty)
  })

  it('should have the correct number of favorites after adding one', () => {
    expect(reducerAction({
      type: constants.ADD_FAVORITE
    })).toEqual(endStateFull)
  })

  it('should update the current weather the user has requested', () => {
    expect(reducerAction({
      type: constants.FETCH_WEATHER_SUCCESS,
      payload: {...mockRequestData2}
    })).toEqual(endStateUpdated)
  })


});
