
import {
  FETCH_WEATHER_BEGIN,
  FETCH_WEATHER_SUCCESS,
  FETCH_WEATHER_FAILURE,
  ADD_FAVORITE,
  DELETE_FAVORITE,
  UPDATE_FAVORITES_BEGIN,
  UPDATE_FAVORITES_FAILURE,
  UPDATE_FAVORITES_SUCCESS
} from '../Constants';

export default function weatherReducer(state, action) {
  switch (action.type) {
    case ADD_FAVORITE:

      const favoritesNoDouble = state.favorites.filter(e => e.name !== state.currentWeather.name)

      return {
        ...state,
        favorites: [...favoritesNoDouble, state.currentWeather]
      }

    case DELETE_FAVORITE:

      const newFavorites = state.favorites.filter(e => e.name !== action.payload.name)
      return {
        ...state,
        favorites: [...newFavorites]
      }
    case FETCH_WEATHER_BEGIN:
      return {
        ...state,
        loading: true
      }
    case FETCH_WEATHER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        currentWeather: {
          name: action.payload.name,
          location: action.payload.coord,
          iconId: action.payload.weather[0].icon,
          temp: Math.round(action.payload.main.temp - 273.15),
          type: action.payload.weather[0].main
        }
      }
    case FETCH_WEATHER_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      }
    case UPDATE_FAVORITES_BEGIN:
      return {
        ...state,
        loading: true,
      }
    case UPDATE_FAVORITES_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        favorites: action.payload.favorites
      }
    case UPDATE_FAVORITES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      }
    default:
      // ALWAYS have a default case in a reducer
      return state;
  }
}
