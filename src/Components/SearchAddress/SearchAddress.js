import * as React from 'react'
import PropTypes from 'prop-types'
import Geosuggest from 'react-geosuggest'
import './styles.css'

export default class AddressInput extends React.PureComponent {

  _onLocationChange(suggestion): void {
    if (suggestion && this.props.onLocationChange) {
      this.props.onLocationChange(Number(suggestion.location.lat),Number(suggestion.location.lng))
    }
  }

  render() {
    return(
      <div>
        <Geosuggest placeholder="Search places..."onSuggestSelect={s => this._onLocationChange(s)}/>
      </div>
    );
  }
}

AddressInput.propTypes = {
  onLocationChange: PropTypes.func
}
