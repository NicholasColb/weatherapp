import * as React from 'react'
import PropTypes from 'prop-types'
import styles from './styles'

export default class WeatherResultsCard extends React.PureComponent {

  render() {
    const { currentWeather } = this.props;
    return(
      <div>
        {currentWeather &&
          <div>
            <p style={styles.text}>
              {currentWeather.name}
            </p>
            <p style={styles.text}>
              {currentWeather.temp}°C
            </p>
            <img src={'http://openweathermap.org/img/w/' + currentWeather.iconId + '.png'} alt="" />
          </div>
      }
      </div>
    );
  }
}

WeatherResultsCard.propTypes = {
  currentWeather: PropTypes.object,
}
