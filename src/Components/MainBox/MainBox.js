import React, { PureComponent } from 'react'
import SearchAddress from '../../Containers/SearchAddress'
import PropTypes from 'prop-types'
import WeatherResultsCard from '../../Containers/WeatherResultsCard'
import styles from './styles'
class MainBox extends PureComponent {
  
  _onFavorite = () => {
      this.props.onFavorite()
      alert("Saved!")
  }

  render() {
    const renderWeather = this.props.currentWeather !== null
    return (
      <div style={styles.container}>
        <p style={styles.header}>Weather App</p>
        <p style={styles.hashtag}>#almaspirit</p>
        <SearchAddress />
        <div style={styles.guideContainer}>
          <p style={styles.guide}>Start typing and select a location from the list</p>
        </div>
        {renderWeather &&
          <div style={styles.weatherContainer}>

            <WeatherResultsCard />


            <button onClick={this._onFavorite} style={styles.saveFavoritesButton}>
              Save to favorites &#10084;
            </button>

            <p style={styles.jokeText}>Do you love this place?</p>

          </div>
        }
      </div>
    );
  }
}

export default MainBox;

MainBox.propTypes = {
    currentWeather: PropTypes.object,
    onFavorite: PropTypes.func
}
