const styles = {
  container: {
    height: '100%'
  },
  header: {
    color:'black',
    textAlign: 'center',
    fontSize: '32px'
  },
  hashtag: {
    marginTop: -20,
    color: 'gray',
    fontSize: '12px'
  },
  guideContainer: {
    margin: '0 auto',
    textAlign: 'left',
    width: '280px'
  },
  guide: {
    marginTop: 5,
    color: 'gray',
    fontSize: '12px'
  },
  weatherContainer: {
    margin: '0 auto',
    marginTop: '30px',
    width: '90%',
    height: '70%',
    border: '2px solid black',
    borderRadius: '5px'
  },
  saveFavoritesButton: {
    width: '120px',
    outline: 'none',
    backgroundColor: '#79f283',
    padding: '8px',
    borderRadius: '5px',
    border: 'none',
    fontSize: '16px'
  },
  jokeText: {
    color: 'gray',
    marginTop: 3,
    fontSize: '12px'
  }

}

export default styles
