import React, { Component } from 'react'
import MainBox from '../../Containers/MainBox'
import Favorites from '../../Containers/Favorites'
import Paper from '@material-ui/core/Paper'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'

import './App.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: 0
    }
  }
  _handleTabChange = (e, value) => {
      this.setState({ value })
  }

  render() {
    const { value } = this.state
    return (
      <div className="App">
        <header className="App-header">
        <div className="Main-container">
          <Paper square>
            <Tabs
              value={value}
              indicatorColor="primary"
              textColor="primary"
              onChange={this._handleTabChange}
            >
            <Tab label="Active" />
            <Tab label="Favorites" />
            </Tabs>
            </Paper>
            {value === 0 && <MainBox />}
            {value === 1 && <Favorites />}

        </div>

        </header>
      </div>
    );
  }
}

export default App;
