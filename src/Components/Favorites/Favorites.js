import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import WeatherResultsCard from '../../Components/WeatherResultsCard'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import styles from './styles'
class Favorites extends PureComponent {
  constructor(props) {
    super(props)
  }

  componentDidMount = () => {
    this.props.updateFavorites(this.props.favorites)
  }
  _deleteFavorite = (fav) => {
    this.props.deleteFavorite(fav.name)
  }
  _renderFavorites = () => {

      return this.props.favorites.map((fav, i) => {
        return (
          <div key={i} style={styles.favoritesContainer}>
            <div style={styles.deleteButtonContainer}>
            <button
              style={styles.deleteButton}
              onClick={() => this._deleteFavorite(fav)}
            >
              <DeleteForeverIcon />
            </button>
            </div>
            <WeatherResultsCard key={i} currentWeather={fav} />
          </div>
        )
      })
    }


  render() {

    return (
      <div style={styles.container}>
        {!this.props.loading && this._renderFavorites()}
      </div>
    );
  }
}

export default Favorites;

Favorites.propTypes = {
  favorites: PropTypes.array,
  loading: PropTypes.bool,
  updateFavorites: PropTypes.func,
  deleteFavorite: PropTypes.func
}
