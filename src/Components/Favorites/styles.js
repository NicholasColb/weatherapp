
const styles = {
  container: {
    height: '100%'
  },
  favoritesContainer: {
    width: '30%',
    margin: '0 auto',
    marginTop: '30px'
  },
  deleteButtonContainer: {
    width: '130%',
    marginBottom: -50,
    backgroundColor: 'transparent',
    textAlign: 'right'
  },
  deleteButton: {
    border: 'none',
    outline: 'none',
    backgroundColor: 'transparent'
  }


}

export default styles;
